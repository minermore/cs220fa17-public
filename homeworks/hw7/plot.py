"""
A quick script to visualize 3D clusters
"""

# imports
import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

"""
Reads in a file returns the data matrix
and cluster labels
"""
def load_file(filename):
    f = open(filename, 'r')
    data = []
    target = []
    for line in f:
        line = line.strip().split('\t')
        point = [float(l) for l in line[0:-1]]
        data.append(point)
        target.append(int(line[-1]))

    f.close()
    return np.array(data), np.array(target)

"""
Plots the clusters and saves to given
filename
"""
def plot_3D_cluster(X, y, filename):
    fig = plt.figure()
    ax = Axes3D(fig, rect=[0, 0, .95, 1], elev=48, azim=134)
    ax.scatter(X[:,0], X[:,1], X[:,2], c=y, edgecolor='k')
    ax.w_xaxis.set_ticklabels([])
    ax.w_yaxis.set_ticklabels([])
    ax.w_zaxis.set_ticklabels([])
    ax.dist = 12
    plt.savefig(filename)

"""
Main
"""
def main():

    if len(sys.argv) < 3:
        print "Error: Usage is python plot.py <input.txt> <output.png>"
        sys.exit(1)

    X, y = load_file(sys.argv[1])
    plot_3D_cluster(X, y, sys.argv[2]);

if __name__ == "__main__":
    main()
