/**
 * Consider the main function and declaration of a populate_array
 * function below. Write the populate_array, next_odd_number, and
 * next_even_number functions so that the main function populates the
 * arrays as intended. Assume the first even number is 2, and first
 * odd number is 1.  Hint: you may want to use static variables.

 * Also write functions to test that populate array is working as intended
 * in conjunction with the other functions.
 */


// A function that loop through the given array
// and populates each element with the provided function
void populate_array(int *array, int array_size, int (*get_next_value)()) {
}

// Provide the next even number
int next_even_number() {
  return 1;
}

// Provide the next odd number
int next_odd_number() {
  return 1;
}

// Provide a random number
// Look up how to produce random
// numbers in C
int random_number() {
  return 1;
}


int main() {
  int size = 5;
  int even_array[size];  // should contain [2, 4, 6, 8, 10]
  int odd_array[size];   // should result in [1, 3, 5, 7, 9]
  int rand_array[size];

  // for now visually inspecting rand_array is fine
  // when we talk about seeding we'll see how we
  // can deterministically test random number generation!
}
