#include <iostream>
#include "RPS.h"

using std::cout;
using std::endl;

// Example main function
// Predict what you expect to occur before you run this!
// You are encouraged to your own additional testing code

int main() {

  Scissors s1(1);
  Paper p1(7);
  Rock r1(15);
  cout << s1.fight(p1) << " " << p1.fight(s1) << endl;
  cout << p1.fight(r1) << " " << r1.fight(p1) << endl;
  cout << r1.fight(s1) << " " << s1.fight(r1) << endl;
  cout << endl;


  //Now putting Scissors object into Tool variable
  Tool t = s1;  //will this call Tool fight or Scissors fight?
  cout << t.fight(p1) << endl;


  //Now making Tool pointer point to Scissors object
  Tool* p = &s1; //will this call Tool fight or Scissors fight?
  cout << p->fight(p1) << endl;


  return 0;
}
