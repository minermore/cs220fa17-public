#ifndef RPS_H
#define RPS_H

/*
  Define class Tool here
*/
class Tool {

 public:
   //Constructors and destructor
   Tool(int str) : strength(new int(str)), type('t') { }  //if no type specified, we make it 't'
   Tool(int str, char t) : strength(new int(str)), type(t) { }
   virtual ~Tool() { }//you fill in

   void setStrength(int str) { } //you fill in
   //int getStrength() { }  //you uncomment and fill in
   //char getType() { } //you uncomment and fill in

   //full definition is given in RPS.cpp, since > 1 line long
   virtual bool fight (Tool& other);

 protected:  //protcted means subclasses have direct access
   int *strength;
   char type;
};


/*
  Define class Rock
*/
class Rock : public Tool {

   public:

   Rock(int str) : Tool(str, 'r') { }

   bool fight (Tool& other);
};

/*
  Define class Paper
*/
class Paper : public Tool {

 public:

  Paper(int str) : Tool(str, 'p') { }

  bool fight (Tool& other);
};


/*
  Define class Scissors
*/ 
class Scissors : public Tool {

 public:

  Scissors(int str) : Tool(str, 's') { }

  bool fight (Tool& other);
};


#endif

