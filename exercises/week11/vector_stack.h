#ifndef VECTOR_STACK_H
#define VECTOR_STACK_H

#include "stack.h"
#include <vector>


/**
 * Vector implementation of a stack
 */
class VectorStack : public Stack {

 public:
  VectorStack(); // default constructor
  VectorStack(const VectorStack &other); // copy constructor
  ~VectorStack(); // destructor

  void push(int i) override; // add a new number to the top of the stack
  int pop() override; // remove top number from the stack and return it
  int top() const override; // return value of the top stack element but don't remove it
  void clear() override; // remove all numbers from the stack
  bool empty() const override; // return true if the stack is empty
  size_t size() const override; // return the number of elements on the stack
  void reserve(size_t len) override; // reallocate storage to have specified capacity

  //output operator should print contents of stack space-separated, on a single line, with
  //bottom stack element on the left end of the line, and top element on the right
  friend std::ostream& operator<< (std::ostream &os, const VectorStack&s); // overload the output operator


 private:
  std::vector<int> data;
};


#endif
