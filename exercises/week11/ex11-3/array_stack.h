#ifndef ARRAY_STACK_H
#define ARRAY_STACK_H

#include "stack.h"


/**
 * Array implementation of a stack
 */
template <typename T>
class ArrayStack : public Stack<T> {

 public:
  ArrayStack(); // default constructor
  ArrayStack(const ArrayStack<T> &other); // copy constructor
  ~ArrayStack(); // destructor

  void push(T i) override; // add a new number to the top of the stack
  T pop() override; // remove top number from the stack and return it
  T top() const override; // return value of the top stack element but don't remove it
  void clear() override; // remove all numbers from the stack
  bool empty() const override; // return true if the stack is empty
  size_t size() const override; // return the number of elements on the stack
  void reserve(size_t len) override; // reallocate storage to have specified capacity

  //output operator should print contents of stack space-separated, on a single line, with
  //bottom stack element on the left end of the line, and top element on the right
  template <typename U>
  friend std::ostream& operator<< (std::ostream &os, const ArrayStack<U> &s); // overload the output operator


 private:
  T *data;
  size_t data_length;
  size_t stack_depth;
};

#include "array_stack.cpp"
#endif
