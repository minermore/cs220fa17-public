#include <iostream>
#include <string>

enum ObjectTypes {
  VAR = 0,
  CONST
};
  
class Object {
 public:
  virtual double value()  const {return 0; }
  virtual std::string name() const { return ""; }
  virtual int type() const = 0;
  virtual ~Object() {}
};

class Variable : public Object {

 public:
  // constructor
 Variable(std::string n) : _name(n) {}

  // get variable name
  std::string name() const {return _name;}

  // get object type
  int type() const { return VAR; }

 private:
  const std::string _name;

};

class Constant : public Object {

 public:

  // non-default constructor
 Constant(double v) : _value(v) {}

  // get value of the variable
  double value() const { return _value; }

  // get object type
  int type() const { return CONST; }

 private:
  const double _value;
};
