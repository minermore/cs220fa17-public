#include "stack.h"
#include "array_stack.h"
#include "object.h"
#include <map>

class RPN_Language {
 public:

  // constructor
  RPN_Language();

  // destructor
  ~RPN_Language();

  // declare variables until BEGIN is encountered
  void read_declarations();
  
  // run code till END is encountered from std::cin
  void run_instructions();

  // calls read declarations and run instructions
  void interpret();

 private:
  // determine if the object is a variable or
  // a constant and return the value by looking
  // at the symbol table or using get_value respectively
  double get_value(const Object *o) const;
  Stack<Object*> *stack;
  std::map<std::string, double> symbol_table;
};

