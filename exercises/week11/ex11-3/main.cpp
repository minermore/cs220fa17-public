#include "rpn_lang.h"
#include <cassert>
#include <string>


int main() {

  RPN_Language interpreter = RPN_Language();

  interpreter.interpret();
  return 0;
}
