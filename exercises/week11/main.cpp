#include "vector_stack.h"
#include <cassert>

void test_stack(Stack &s) {

  assert(s.empty());

  s.push(1);
  assert(!s.empty());
  assert(s.top() == 1);

  s.push(2);
  assert(s.size() == 2);
  assert(s.top() == 2);

  assert(s.pop() + s.pop() == 3);
  assert(s.empty());

  try { s.pop(); }

  catch (std::underflow_error &e) {
    std::cerr << e.what() << std::endl;
  }
  
  try { s.top(); }

  catch (std::underflow_error &e) {
    std::cerr << e.what() << std::endl;
  }

  for (int i = 1; i <= 100; i++) {
    s.push(i);
  }
  assert(s.pop() == 100);
  assert(s.size() == 99);

}

int main() {


  // tests for VectorStack
  // VectorStack vs = VectorStack();
  // test_stack(vs);
  // std::cout << vs << std::endl;

  // tests for ArrayStack
  // ArrayStack as = ArrayStack();
  // test_stack(as);
  // std::cout << as << std::endl;
  return 0;
}
