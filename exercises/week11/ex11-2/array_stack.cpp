#include "array_stack.h"
#include <exception>


/**
 * Array implementation of a stack
 */

// default constructor
ArrayStack::ArrayStack() {
  data_length = 5;
  stack_depth = 0;
  data = new int[data_length];
}

// copy constructor
ArrayStack::ArrayStack(const ArrayStack &other) {
  data_length = other.data_length;
  stack_depth = other.stack_depth;
  data = new int[data_length];

  for (size_t i = 0; i < stack_depth; i++) {
    data[i] = other.data[i];
  }
}

// destructor
ArrayStack::~ArrayStack() {
  delete[] data;
}

// add a new number to the top of the stack
void ArrayStack::push(int i) {
  // check if we're full
  if (stack_depth >= data_length) {
    reserve(data_length * 2);
  }
  data[stack_depth++] = i;
}

// remove top number from the stack and return it
int ArrayStack::pop() {
  if (empty()) {
    throw std::underflow_error("Popping empty stack");
  }

  // NOTE: this MUST be pre-increment NOT post
  return data[--stack_depth];
}

// return value of the top stack element but don't remove it
int ArrayStack::top() const {
  if (empty()) {
    throw std::underflow_error("Topping empty stack");
  }
  return data[stack_depth-1];
}

// remove all numbers from the stack
void ArrayStack::clear() {
  stack_depth = 0;
}

// return true if the stack is empty
bool ArrayStack::empty() const {
  if (stack_depth == 0) {
    return true;
  }
  return false;
}

// return the number of elements on the stack
size_t ArrayStack::size() const {
  return stack_depth;
}

// reallocate storage to have specified capacity
void ArrayStack::reserve(size_t len) {
  if (len < data_length) {
    throw std::length_error("Resize length cannot be smaller than original length");
  }
  // create a temp holder for old data
  int *tmp = data;
  data = new int[len];
  for (size_t i = 0; i < stack_depth; i++) {
    data[i] = tmp[i];
  }
  delete[] tmp; // free old data
  data_length = len; // update data length
}


// output operator should print contents of stack space-separated,
// on a single line, with bottom stack element on the left end
// of the line, and top element on the right
// overload the output operator
std::ostream& operator<< (std::ostream &os, const ArrayStack&s) {
  for (size_t i = 0; i < s.size(); i++) {
    os << s.data[i] << " ";
  }
  return os;
}
