#ifndef STACK_H
#define STACK_H

#include <iostream>


/**
 * The base stack class.
 * Stacks are first-in-last-out data structures, meaning you
 * can only add or remove elements at the top of the stack

 * Notice that you never instantiate objects of this
 * base class, only ones of its derived classes.
 */
class Stack {

 public:
  virtual ~Stack() {} // destructor, NOTE : destructors must be virtual if you want the derived classes' destructor being called when using polymorphism

  virtual void push(int i) = 0; // add a new number to the top of the stack
  virtual int pop() = 0; // remove top number from the stack and return it
  virtual int top() const = 0; // return value of the top stack element but don't remove it
  virtual void clear() = 0; // remove all numbers from the stack
  virtual bool empty() const = 0; // return true if the stack is empty
  virtual size_t size() const = 0; // return the number of elements on the stack
  virtual void reserve(size_t len) = 0; // reallocate storage to have specified capacity
};


#endif
