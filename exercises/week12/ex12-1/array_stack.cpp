#include "array_stack.h"
#include <exception>


/**
 * Array implementation of a stack
 */

// default constructor
template <typename T>
ArrayStack<T>::ArrayStack() {
  data_length = 5;
  stack_depth = 0;
  data = new T[data_length];
}

// copy constructor
template <typename T>
ArrayStack<T>::ArrayStack(const ArrayStack &other) {
  data_length = other.data_length;
  stack_depth = other.stack_depth;
  data = new T[data_length];

  for (size_t i = 0; i < stack_depth; i++) {
    data[i] = other.data[i];
  }
}

// destructor
template <typename T>
ArrayStack<T>::~ArrayStack() {
  delete[] data;
}

// add a new number to the top of the stack
template <typename T>
void ArrayStack<T>::push(T i) {
  // check if we're full
  if (stack_depth >= data_length) {
    reserve(data_length * 2);
  }
  data[stack_depth++] = i;
}

// remove top number from the stack and return it
template <typename T>
T ArrayStack<T>::pop() {
  if (empty()) {
    throw std::underflow_error("Popping empty stack");
  }

  // NOTE: this MUST be pre-increment NOT post
  return data[--stack_depth];
}

// return value of the top stack element but don't remove it
template <typename T>
T ArrayStack<T>::top() const {
  if (empty()) {
    throw std::underflow_error("Topping empty stack");
  }
  return data[stack_depth-1];
}

// remove all numbers from the stack
template <typename T>
void ArrayStack<T>::clear() {
  stack_depth = 0;
}

// return true if the stack is empty
template <typename T>
bool ArrayStack<T>::empty() const {
  if (stack_depth == 0) {
    return true;
  }
  return false;
}

// return the number of elements on the stack
template <typename T>
size_t ArrayStack<T>::size() const {
  return stack_depth;
}

// reallocate storage to have specified capacity
template <typename T>
void ArrayStack<T>::reserve(size_t len) {
  if (len < data_length) {
    throw std::length_error("Resize length cannot be smaller than original length");
  }
  // create a temp holder for old data
  T *tmp = data;
  data = new T[len];
  for (size_t i = 0; i < stack_depth; i++) {
    data[i] = tmp[i];
  }
  delete[] tmp; // free old data
  data_length = len; // update data length
}


// output operator should print contents of stack space-separated,
// on a single line, with bottom stack element on the left end
// of the line, and top element on the right
// overload the output operator
template <typename T>
std::ostream& operator<< (std::ostream &os, const ArrayStack<T> &s) {
  for (size_t i = 0; i < s.size(); i++) {
    os << s.data[i] << " ";
  }
  return os;
}
