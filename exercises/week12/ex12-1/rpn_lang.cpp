#include "rpn_lang.h"
#include <cctype>
#include <exception>
#include <sstream>

// constructor
RPN_Language::RPN_Language() {
  stack = new ArrayStack<Object*>();
}

// destructor
RPN_Language::~RPN_Language() {
  while (!stack->empty()) {
    delete stack->pop();
  }
  delete stack;
}

double RPN_Language::get_value(const Object *o)  const {
  if (o->type() == VAR) {
    return symbol_table.at(o->name());
  }
  return o->value();
}


// read declarations till BEGIN is encountered
void RPN_Language::read_declarations() {
  double value;
  std::string name = "";

  // keep reading declarations
  // line by line
  for (std::string line; std::getline(std::cin, line);) {

    // till BEGIN
    if (line == "BEGIN") {
      break;
    }

    std::istringstream iss = std::istringstream(line);

    // put into symbol table
    iss >> name;
    iss >> value;
    symbol_table[name] = value;
  }
}


// run instructions till END is encountered
void RPN_Language::run_instructions() {
  std::string input = "";

  // keep reading instructions
  while (true) {

      // if we see end, break
      std::cin >> input;
      if (input == "END") {
	break;
      }
    
      // check if it's a number
      if (isdigit(input[0])) {
	// if it is, push a constant
	stack->push(new Constant(atof(input.c_str())));
      }

      // check if the user is trying to print
      else if (input == "PRINT") {
	Object *o = stack->pop();
	std::cout << get_value(o) << std::endl;
	delete o;
      }

      // check if it's name of a variable
      else if (isalpha(input[0])) {
	// if it is, push a pointer to the variable
	if (symbol_table.find(input) == symbol_table.end()) {
	  throw std::out_of_range("Undeclared variable " + input);
	}
	stack->push(new Variable(input));
      }

    
      // check if it's a legal operator in our language
      else if (input == "+" || input == "=") {


	// it's an operator so pop twice

	// TODO : handle exceptions pertaining to not enough operands on the stack
	// Recall that the stack will throw an exception already if try to pop when empty
	Object *right = stack->pop();
	Object *left = stack->pop();

	if (input == "=") {
  	  // check if value getting assigned a value here was previously declared
	  if (symbol_table.find(left->name()) == symbol_table.end()) {
	    throw std::out_of_range("Undeclared variable " + input);
	  }
  	  // assign the new value
	  symbol_table[left->name()] = get_value(right);
	}

	else if (input == "+") {
	  stack->push(new Constant(get_value(left) + get_value(right)));
	}

	// TODO : add implementation of -, *, /

	delete left;
	delete right;
      }

      // user has entered a symbol that is not in the language
      // syntax
      else {
	throw std::invalid_argument("Unrecognized symbol " + input);
      }

  }
}

// declare variables until BEGIN is encountered
// run code till END is encountered from std::cin
void RPN_Language::interpret() {

  // read decls
  read_declarations();

  // run instructions
  run_instructions();
}


