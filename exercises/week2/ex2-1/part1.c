#include <stdio.h>
#include <string.h>

/* 
    In class exercise Ex-2-1.
    Practice using string functions and writing helper methods.
*/



/*
    Returns in the third argument the concatenation of the first
    argument and the second argument, provided that there is
    sufficient space in third argument, as specified by the fourth.
    e.g.
        concat("alpha", "beta", result, 10) puts "alphabeta" into result and returns 0
        concat("alpha", "gamma", result, 10) puts nothing into result and returns 1

 */
int concat(char word1[], char word2[], char result[], int result_cap){


    /* FILL IN YOUR CODE HERE */

}


int main() {

    char word1[11];  //allow up to 10 chars, then room for '/0' terminator
    char word2[11];  //allow up to 10 chars, then room for '/0' terminator

    //collect two strings (doesn't prompt first)
    scanf("%s", word1);
    scanf("%s", word2);

    //collect an integer capacity (doesn't prompt)
    int result_cap;
    scanf("%d", &result_cap);
    char result[result_cap];

    //call concatenation function and report result based on returned value
    if(!concat(word1, word2, result, result_cap)) {
        printf("Concatenation was successful: %s\n", result);
    } else {
        printf("Concatenation was not successful.\n");
    }

    return 0;
}

