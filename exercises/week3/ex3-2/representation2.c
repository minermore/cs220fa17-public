//600.220 Exercise 3-2 starter code

#include <stdio.h>

int main() {

    //declare a char and output it in various other formats
    char c = 'A';
    printf("\ncharacter c is %c\n", c);
    printf("character c as an int is %d\n", c);
    printf("character c as a hex value is %x\n", c);
    printf("character c plus 4 is %c\n", c+4);
    printf("character c plus 4 as an int is %d\n", c+4);
    printf("character c plus 4 as a hex value is %x\n", c+4);

    //play around with a double variable
    double val = 3.2;
    printf("\nvalue is %f\n", val);
    printf("value to more places is %19.17f\n", val);
    if (val == 9.6 / 3) {
        printf("3.2 and 9.6 / 3 are equal!\n");
    } else {
        printf("3.2 and 9.6 / 3 are NOT equal!\n");
    }

    return 0;
}

