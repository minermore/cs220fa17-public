/**
 * Here is a list of all the imports you'll need
 * iostream for IO with stdin/stdout/stderr
 * fstream for creating IO streams for files
 *     ifstream is used for input file streams (reading)
 *     ofstream is used for output file streams (writing)
 *     think of these as replacements for fscanf and fprintf
 * sstream for creating IO streams for strings
 *     istringstream is used for input string streams
 *     ostringstream is used for output string streams
 *     think of these as replacements for sscanf and sprintf
 * Remember these are classes so you can for example create an ifstream as
 *     std::ifstream my_stream; my_stream.open("filename.txt"); my_stream.close();
 * You can also create one using a constructor that takes in the filename as
      std::ifstream my_stream("filename.txt");
 * You can then read (not write, remember this is a read only stream) using the >> operator
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>

using std::cout;
using std::endl;
using std::ifstream;
using std::istringstream;
using std::string;
using std::getline;
using std::vector;
using std::map;


// Counts the number of words on each line of 
// a given file. Creates a vector in which
// to store the counts, and returns the vector.
vector<int> read_int_vector(string filename) {

    vector<int> result;



    // **** FILL IN YOUR CODE HERE ****



    return result;
}



// Counts the number of wordss on each line of 
// the given file. Creates a map associating
// each successive line number with the count
// of values on that line, and returns the map.
map<int, int> read_int_int_map(string filename) {

    map<int, int> result;



    // **** FILL IN YOUR CODE HERE ****



    return result;
}

// Creates map which associates each successive line
// number with a different vector of the strings stored
// on that line number in the file, and returns the map.
map<int, vector<string>> read_int_vec_map(string filename) {

    map<int, vector<string>> result;



    // **** FILL IN YOUR CODE HERE ****



    return result;
}

// Outputs the contents of an int vector, with
// individual elements separated by spaces, then
// ends the line.
void print_int_vector(const vector<int> vec) {
    for (unsigned int i = 0; i < vec.size(); i++) {
	cout << vec[i] << " ";
    }
    cout << endl;
}

// Outputs the contents of a map<int, int> type.
// Associated items are separated by a single space,
// and different pairs are separated by a single tab.
// Once the map is output, the line is ended.
void print_int_int_map(const map<int, int> m) {

    for (map<int, int>::const_iterator i = m.cbegin(); i != m.cend(); i++) {
	cout << i->first << " " << i->second << "\t";
    }
    cout << endl;
}


// Outputs the contents of a map<int, vector<string>> type,
// one pair per line. On a single line, the key integer
// is output first, followed by a tab character, then the 
// associated vector elements are output separated by
// single spaces on that same line.
void print_int_vec_map(const map<int, vector<string>> m) {



  // **** FILL IN YOUR CODE HERE ****                                                                    



}


int main() {

    cout << "Hello world" << endl;


    vector<int> result1 = read_int_vector("data.txt");
    print_int_vector(result1);
    cout << endl;


    map<int, int> result2 = read_int_int_map("data.txt");
    print_int_int_map(result2);
    cout << endl;

    map<int, vector<string>> result3 = read_int_vec_map("data.txt");
    print_int_vec_map(result3);

}
