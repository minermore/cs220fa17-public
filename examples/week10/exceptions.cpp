/************************************************************
 * exceptions.cpp
 *
 * Basic demonstration of using exceptions in C++.
 ************************************************************/

#include <iostream>
#include <vector>
#include <exception> // base exception type
#include <stdexcept> // assorted sub-types

using std::cout;

void foo(int &x); // defined below main()
int bar(int x);

/* we can make our own exception types by extending
 * std::exception: */
class Boom : public std::exception {
  /* override the what() method to print a custom message */
  virtual const char* what() const noexcept override {
    return "BOOM!\n";
  }
};


int main() {

  /* unhandled exceptions will end the program 
   * (uncomment the next line to try it) */
  //throw std::exception();

  /* we need to handle them using try..catch blocks: */
  try {
    /* code that might cause an exception goes inside "try { }" */
    cout << "a\n";
    throw std::exception();
    /* throw is like return, in that it immediately stops running the 
     * current block of code, and starts moving up the call stack until
     * it finds a matching catch() statement (or until it gets to the top,
     * in which case it ends the program) */
    cout << "b\n";
    /* catch () says we're preared to handle exceptions 
     * (of the specified type).  Note that we use a reference
     * here, which lets us take advantage of polymorphism
     * (so if we see an exception that's a sub-type of 
     * std::exception, this code will still catch it) */
  } catch (std::exception &ex) { 
    /* code to run if we caught an exception */
    cout << "caught exception!\n";
  }

  /* we can have multiple catch statements for a single try; the first catch
   * block that matches the thrown exception (where "matches" refers to
   * polymorphic type matching) is the one that will get run: */
  try { throw std::overflow_error("ran out of space!\n");

  } catch (std::invalid_argument &e) {
    cout << "got invalid argument: e.what()=" << e.what() << "\n";
  } catch (std::overflow_error &e) {
    cout << "got overflow exception: e.what()=" << e.what() << "\n";
  } catch (std::exception &e) {
    cout << "got base exception: e.what() = \"" << e.what() << "\"\n";
  }
  /* Make sure you put the most-specific catch statements at the top;
   * if you put the general ones first, it will just run them and never
   * get to the specific ones. */


  /* we can also catch exceptions that get thrown in functions, which 
   * will basically shoot up the call stack (like doing an immediate
   * "return" from every function) until it finds a try block with a
   * catch statement matching the thrown object. */
  try {
    int num = 10;
    foo(num);
    cout << "(1) num = " << num << "\n";
    foo(num);
    cout << "(2) num = " << num << "\n";
    foo(num);
    cout << "(3) num = " << num << "\n";
  } catch (std::range_error &e) { 
    cout << " caught range error: " << e.what() << "\n";
  }


  /* lots of standard library functions throw exceptions 
   * when things go wrong.  For example, you don't actually need
   * to check for a null pointer after calling new, because
   * if it fails it throws an exception */
  try {
    int *array = new int[100000000000]; // try to allocate ~400 terabyte long array
    array[0] = 10; // in C, this would probably segfault, because when malloc() fails to get space it just returns a null pointer
  } catch (std::bad_alloc &bae) {
    cout << "error while allocating: " << bae.what() << "\n";
  }

  /* for another example, the vector .at() method throws an exception
   * if you try to access something that's outside the allocated region: */
  try{
    std::vector<double> v(10); // vector 10 elements long
    /* NOTE: remember that .at() does checking, but the [] operator does not */
    v.at(9) = 20; // fine
    v.at(10) = 21; // out-of-range

  } catch (std::exception &ex) { // note that what gets thrown is a subtype, but polymorphism helps us out...
    cout << "error with vector: " << ex.what() << "\n";
  }

  /* we can throw and catch our own custom exceptions as well */
  try {
    Boom firecracker;
    throw firecracker; // remember, we're throwing an object; it doesn't have to be instanciated on the same line as the throw
  } catch (std::exception &e) {
    cout << "e.what: " << e.what() << "\n";
  } catch (Boom &b) {
    /* notice that this code will never run, since anything
     * of type Boom& will match std::exception& and get handled
     * by the previous catch block */
    cout << "WILL NEVER GET HERE\n";
  }

  /* we can technically throw anything, but it's usually a 
   * bad idea... */
  try {
    throw 7;
  } catch (int &i) {
    /* this works, but it's considered poor style to throw/catch
     * things that aren't descended from std::exception */
    cout << "caught: " << i << "\n";
  }

  cout << "Got to the end, exiting cleanly...\n";
  return 0;
}

/******************************************/

void foo(int &x) {
  cout << "entered foo, x = " << x << "\n";
  for (int i=0; i<3; i++) {
    x = bar(x);
  }
}

/******************************************/

int bar(int x) {
  cout << "entered bar, x = " << x << "\n";
  if (x > 100000) {
    /* many exception types have a constructor that takes a string,
     * which will get used as part of .what() */
    throw std::range_error("x out of range ( " + std::to_string(x) + " > 100000)");
  } 

  return x * 10;
}


