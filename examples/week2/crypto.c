/**
 * To compile use gcc -Wall -Wextra -pedantic -std=c99 crypto.c
 * To run use ./a.out message.txt encoded.txt
 * The two files can be replaced with any files you like

 * This file was an in-class demo to demonstrate some uses of
 * file IO, 2D arrays, bitwise operations, and modular thinking.

 * In addition to these, we've also added some code to demonstrate
 * "random" number generation in C (something that proves to be
 * useful regardless of what field you might find yourself in
 * the future but is especially important in machine learning and cryptography!) 
 * The method shown here is still not ideal but definitely a whole lot better than
 * the passcode generation scheme shown in class which just used what number
 * character we were on as the passcode (a very easy code to break!).
 * Maybe you want to experiment with your own encryption schemes or find other
 * ways to make this program better :-D

 * Finally, we've also added taking in command line arguments so that
 * the user may specify the path to the files being read in, and written to.

 * This is a program that takes a file specified by the user
 * and encodes the message in the file using bitwise XOR
 * operations with "randomly" generated one-time passcodes. 
 * This encoded message is written out to a file, and then
 * decoded using a similar scheme with the exact same passcodes
 * that were used for encoding. The decoded message is written to stdout.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h> // for the rand function
#include <time.h> // for the time function

#define MAX_LENGTH 255
#define MAX_MSG_LENGTH 100

/**
 * Encode a given input by mapping each character of the input
 * using bitwise XOR operation between the character and randomly
 * generated one time passcode. Write the encoded output to a file.
 *
 * @param fp file pointer to output file
 * @param input 2D char array i.e. array of strings
 * @param key 2D integer array that will be populated with the one time
 *        passcodes used for each character
 * @param num_words number of words in the input
 */
void encode(FILE *fp, char input[][MAX_LENGTH], int key[][MAX_LENGTH], int num_words) {
  // int num_chars = 0; old one-time passcodes
  for (int i = 0; i < num_words; i++) {
    char word[MAX_LENGTH];
    for (unsigned int j = 0; j < strlen(input[i]); j++) {
      // old OTP scheme that was easily guessable
      // key[i][j] = num_chars;
      // word[j] = input[i][j] ^ num_chars++;
      
      // new better one, generate a random number between 0 and 10
      key[i][j] = rand() % 10;
      word[j] = input[i][j] ^ key[i][j];
    }
    word[strlen(input[i])] = '\0';
    fprintf(fp, "%s\n", word);
  }
}


/**
 * Decode a given input by mapping each character of the input
 * using bitwise XOR operation between the character and the corresponding
 * integer key that served as its one-time passcode. Writes the decoded
 * output to stdout
 *
 * @param fp file pointer to output file
 * @param input 2D char array i.e. array of strings
 * @param key 2D integer array that has been populated with the one time
 *        passcodes used for each character during the encoding phase
 * @param num_words number of words in the input
 */
void decode(char input[][MAX_LENGTH], int key[][MAX_LENGTH], int num_words) {
  for (int i = 0; i < num_words; i++) {
    char word[MAX_LENGTH];
    for (unsigned int j = 0; j < strlen(input[i]); j++) {
      word[j] = input[i][j] ^ key[i][j];
    }
    word[strlen(input[i])] = '\0';
    printf("%s\n", word);
  }
}

/**
 * Loads a message from an input file
 * @param fp is a file pointer
 * @param input is a array of strings
 * @return integer denoting how many words were read
 */

int load(FILE *fp, char input[][MAX_LENGTH]) {

  char word[MAX_LENGTH];
  int i = 0;
  while (fscanf(fp, "%s", word) == 1) {
    strcpy(input[i++], word);
  }
  return i;
}

/**
 * The main function

 * @param argc Number of arguments provided to the program
          argc is always at least 1 i.e. the name of the program itself
 * @param argv array of strings (char*) containing the user specified
          command line arguments. argv[0] is the name of the program itself
 */
int main(int argc, char* argv[]) {

  // check command line arguments
  if (argc < 3) {
    printf("Usage: ./a.out <source> <destination>\n");
    exit(-1); // exit code for failure
  }

  // set the "seed" of our random number generator
  // if you'd like to here more about this, I'd be happy
  // to discuss in class/office hours
  srand(time(NULL));
  
  // loading the file
  // NOTE: a good program should also do checks
  // to see if the file exists and not just seg fault
  FILE *message_file = fopen(argv[1], "r");
  char input[MAX_MSG_LENGTH][MAX_LENGTH];
  int num_words = load(message_file, input);
  fclose(message_file);

  // writing encoded message
  FILE *encoded_file = fopen(argv[2], "w");
  int key[MAX_MSG_LENGTH][MAX_LENGTH];
  encode(encoded_file, input, key, num_words);
  fclose(encoded_file);

  // decrypt message
  FILE *input_file = fopen("encoded.txt", "r");
  num_words = load(input_file, input);
  decode(input, key, num_words);
  return 0;
}
