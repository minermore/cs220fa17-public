#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>

/****************************************************
 * C language reference and example file.
 * Compile with the following command:
 * gcc -std=c99 -Wall -Wextra -pedantic -Wno-unused-variables -Wno-unused-values language.c
 * and then run with:
 * ./a.out
 */

/* block comments are any text appearing between slash-star and star-slash */

/* block comments can be on one line, but can also span
 * multiple lines if desired.  Putting a * at the start of
 * later lines is 
 optional, but it's nice visually, and many
 * editors will do it automatically.
 */

// inline comments start with two slashes, and end at the end of the line

// all C programs must have a main() function; it's where execution starts
// main() should always return an int
int main() {

  /*******************************************************************/
  /* OUTPUT: 
   * The simplest way of printing output in C is the printf() function,
   * which we get from <stdio.h>
   *
   * printf() takes a string (enclosed in double quotes), and prints that
   * string to the terminal.  Note that it doesn't automatically print
   * a linebreak; be sure to put a "\n" at the end of your string if you
   * want a linebreak to happen.
   *
   * You can also include special "formatting" placeholders in your string, 
   * and then pass extra arguments that match those placeholders to printf().
   * This lets you print the current value a variable, for example.
   */
  printf("This is a string printed with no linebreak at the end.");
  printf("This is a string printed with a linebreak at the end.\n");

  printf("Here is an integer: %d, and a floating point number: %f\n", 4, 3.14);

  /*******************************************************************/
  /* PRIMITIVE TYPES: 
   * C is a strongly typed language.  All variables must be declared, with
   * a type specified, before use.
   * 
   * The size of each type is determined by the compiler, not the language,
   * so it may vary by platform (e.g. really old computers may use less bits
   * for an 'int' than modern ones).
   *
   * These days, 'int' is usually 32-bits (equivalent to 4 bytes)
   */
  int a; // basic integer type
  short b; // smaller integer type; 'short int b' is equivalent
  long c; // bigger integer type; 'long int c' is equivalent 
  char d; // character type; actually considered a (small) integer-type in C

  //To find out how big a type is (in bytes) use sizeof(), e.g.:
  printf("size of int is: %ld byte(s)\n", sizeof(int));
  printf("size of char is: %ld byte(s)\n", sizeof(char));
  printf("size of long is: %ld byte(s)\n", sizeof(long));

  /* any integer type (i.e. any of the above) can be declared as 'unsigned'
   * which means it can't have values less than 0, but effectively doubles
   * the largest positive value it can hold
   */
  unsigned int e; 
  unsigned char f;

  /* floating point types; 'bigger' here means more precision
   * rather than expanded range.
   */
  float g; // basic floating point type (single-precision)
  double h; // bigger foating point type (double-precision)
  long double i; // even bigger floating point type (quadruple-precision)
   /* NOTE: most people use 'double' as the "default" floating point
   * type.  Use of 'float' and 'long double' is rare, and reserved
   * for special circumstances.
   */

  /* Boolean types weren't part of the C standard until C99, and
   * require a special include (namely, <stdbool.h>) to use.
   *
   * Historically, people just used 'int's, with 0=false and 1=true.
   */
  bool j; // boolean type


  /*******************************************************************/
  /* DECLARATIONS:
   * Variables must be declared with types before use.  Multiple variables
   * can be declared on one line if they are the same type.
   * e.g.: */
  int k, l, m;

  /* Variables can also be declared and initialized at the same time, e.g.*/
  int n=3;

  /* While it is possible to declare and initialize multiple values on the
   * same line, it is generally considered poor style, and should be avoided,
   * e.g.*/
  int o=0, p=4;

  printf("o is %d, p is %d\n", o, p);

  /* Variables which are not initialized have "junk" values in them, which may
   * change when the program is re-run.  Be sure to assign something to
   * your variables before you read from them.
   */

  /*******************************************************************/
  /* ARRAYS:
   * arrays in C work similarly to other languages, but C doesn't
   * check to make sure you don't try to access elements outside the
   * array (this sometimes crashes your program, and sometimes does
   * even worse things).
   *
   * Size is fixed at declaration time; you can't declare it without
   * a size and then fill in the size later, and the size can't be
   * changed once it's been declared (though you can use a variable,
   * so the size may be different each time the program is run)
   */
  int q[3]; // an array of 3 integers
  int r[4] = {5, 24, 8, 72}; // an array of 4 integers, with initialized values
  int s[] = {5, 24, 8, 72}; // size automatically determined from initializer
  /* Array indexing starts from 0, so valid indicies for a length
   * 4 array are 0, 1, 2, 3
   */

  /*******************************************************************/
  /* LITERALS:
   * these are hard-coded "literal" values in your C program.
   */

  int t = 7; // 7 is an integer literal
  double u = 7.0; // 7.0 is a double-precision floating-point literal
  float v = 7.0f; // 7.0f single-precision floating-point literal; rarely used

  char w = 'a'; // character literals must be surrounded by single quotes

  /*******************************************************************/
  /* STRINGS:
   * Strings in C are really just arrays of type char; there
   * is no string "type".  Because of this, you can't apply most
   * operators to strings; things like testing for equality or
   * string concatenation require special functions (see below).
   *
   * A char array used as a string must be "null-terminated",
   * meaning the character '\0' must appear at the end.  Be
   * sure your array is long enough to hold the null character!
   */
  char x[5] = {'w','o','r','d','\0'};

  /* String literals are character arrays; C will 
   * automatically null-terminate  a string literal, so 
   * the array y (below) will be identical to the array x * (above).
   */
  char y[] = "word"; // string literals must be surrounded by double quotes

  /* String-manipulation functions can be found in the <string.h> library;
   * if you include that library, you can use things like strlen: */

  int z = strlen(y); // z will be the "length" of the string

  /* NOTE: strlen counts the number of characters before the first '\0',
   * which is *NOT* the same as the length of the array.  It will always
   * be at least 1 less than the length of the array, and could be much
   * less if the string has a '\0' before the final array element.
   *
   * See string library reference for more string functions, including
   * copying strings, concatenating strings, and comparing strings for equality
   * or lexical ordering.  Be careful when you copy or concatenate that the
   * destination is a big enough array to hold the result!
   */


  /* ctypes.h: this library contains functions for working with characters,
   * including things like checking or changing the case of letters, e.g.: */
  isalpha('a'); // will evaluate to "true"
  tolower('A'); // will evaluate to 'a'

  /*******************************************************************/
  /* OPERATORS:
   * most work similarly to other languages, e.g.: */
  int aa = 7 + 4 - 3 * 9 / (5 % 2); // see reference for order of operations

  /* assignment can be combined with arithmetic:*/
  aa += 6; // equivalent to 'aa = aa + 6'
  aa -= 3;
  aa *= 9;
  aa /= 4;
  aa %= 2;

  /* can also increment/decrement with a unary operator:*/
  aa++; // equivalent to aa = aa + 1;
  aa--; // equivalent to aa = aa - 1;

  /* relational testing operators: */
  aa < a; // true if aa is less than a
  aa > a; // true if aa is greater than
  aa <= a; // true if aa is less than or equal to a
  aa >= a; // true if aa is greater than or equal to a
  aa == a; // true if a and aa have the same value
  aa != a; // true if a and aa have different values

  /* logical operators: */
  bool ab = true;
  bool ac = false;
  !ab; // negation (evaluates to false in this case)
  ab || ac; // inclusive-or (evaluates to true in this case)
  ab && ac; // and (evaluates to false in this case)


  /*******************************************************************/
  /* IF/ELSE:
   * For if/else statements, a single statement is done by default; if
   * you want to do more than one statement, use a block (i.e. curly
   * braces).  As a matter of good style and practice, it's a good
   * habit to just always use braces even when you don't strictly need
   * them.
   */

  // can use a bool type variable
  if (aa)
    printf("aa is true\n");
  else
    printf("aa is false\n");

  // can also use any integer type; 0 will be interpreted as
  // false, and *anything else* will be interpreted as true
  a = 0;
  if (a) {
    printf("0 is true\n");
  } else {
    printf("0 is false\n");
  }

  if(-1){
    printf("-1 is true\n");
  }

  // can also use any expression that evaluates to an int type or a bool
  if ( 4 % 2 == 0 ) {
    printf("4 is an even number\n");
  } else {
    printf("4 is an odd number\n");
  }
  
  //NOTE: the assignment operator evaluates to the assigned value, e.g.
  if (a == 1) 
    printf("a is 1\n");
  else
    printf("a is not 1\n");

  if (a = 1) // a has its value changed to 1, and also the if() sees 1
    printf("a changed to 1\n");

  if (a == 1)  
    printf("a is still 1\n");

  /* can chain if/else together: */

  a = 0;
  b = 1;
  c = 0;
  if (a) {
    printf("will print if a is true\n");
  } else if (b) {
    printf("will print if a is false but b is true print\n");
  } else if (c) {
    printf("will print if a and b are false but c is true\n");
  } else {
    printf("will print if a and b and c are all false\n");
  }

  /*******************************************************************/
  /* SWITCH:
   * C also lets you use a "switch" statement, which can be nice
   * if you want lots of cases.  The variable you switch on must
   * be an integer type.
   */
  b = 2;
  switch (b) {
    case 1: // literal value, checked for equality with switch variable
      printf("b was 1\n");
      break; // the break statement jumps to the end of the switch{} block
    case 2:
      printf("b was 2\n");
      // with no break statement, we keep executing the following case
    case 3:
      printf("b was 3 (or 2)\n");
      break; // again, break stops us
    default:
      printf("none of the above cases matched\n");
  }


  /*******************************************************************/
  /* LOOPS:
   * C has 3 loop constructs, all of which work much like they do in
   * Java.  The three loops below are equivalent:
   */

  printf("while loop\n");
  a = 0;
  while (a < 5) {
    printf("a is %d\n", a);
    a++;
  }

  // do...while checks at the end of the loop, instead of the beginning
  printf("do-while loop\n");
  a = 0;
  do {
    printf("a is %d\n", a);
    a++;
  } while (a < 5);

  printf("for loop\n");
  for (a = 0; a < 5; a++) {
    printf("a is %d\n", a);
  }

  return 0; // return from main exits program; return 0 on normal exit
}
