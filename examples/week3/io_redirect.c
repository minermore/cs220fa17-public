#include <stdio.h>

int multiply(int x) {
  return x*5;
}

int add(int x) {
  return x+5;
}

int subtract(int x) {
  return x-5;
}

int main() {

  int option;
  int input;

  while (scanf("%d %d", &option, &input) == 2) {
    switch(option) {
    case 1:
      printf("%d\n", multiply(input));
      break;
    case 2:
      printf("%d\n", add(input));
      break;
    case 3:
      printf("%d\n", subtract(input));
      break;
    }
  }
  return 0;
}
