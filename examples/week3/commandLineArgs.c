/*****************************************
 * commandLineArgs.c
 *
 * Demonstration of how to deal with command-line
 * arguments in a C program.
 * Based on a file originally created by Ben Mitchell.
 *
 * Compile with: gcc -std=c99 -pedantic -Wall -Wextra commandLineArgs.c
 *
 * Run with: ./a.out [optional arguments]
 *    (try : ./a.out input_file.txt
 *****************************************/
#include <stdio.h>
#include <stdlib.h>

/* to use command-line args, we need to have our
 * prototype for main look like this: */
int main(int argc, char* argv[]) {
  /* Note that the names argc and argv are not technically
   * required, but they are conventional, and you should
   * stick with that convention.  The types and order are
   * important, though you may see char** argv instead of 
   * char* argv[] in some programs; the two forms actually
   * mean pretty much the same thing, as we'll learn next week.
   */

  /* argc contains the total number of "arguments", which
   * is basically how many (whitespace-delimited) words were
   * typed on the command line */
  printf("total number of arguments: %d\n", argc);

  /* argv is an array of C-strings (i.e. null-terminated char arrays)
   * each element is one word that was typed on the command line.
   * Since it contains the litteral command typed, the first "argument"
   * will always be the name of the executable that was run: */
  printf("we were called as: \'%s\'\n", argv[0]);

  /* let's just print out all the arguments as strings: */
  printf("args were:\n");
  for (int i=0; i<argc; i++) {
    printf("\t%s\n", argv[i]);
  }

  /* We can use these strings directly, e.g. as filenames: */
  if (argc > 1) { // make sure the argument we're about to read exists
    FILE *infile = fopen(argv[1], "r");
    if (!infile) {
      printf("error: failed to open file \"%s\"\n", argv[1]);
    } else {
        //read in something from file
        char value[255];
        fscanf(infile, "%s", value);
        printf("Value read in from file: %s\n", value);
        fclose(infile);
    }
  }



  return 0;
}
